# Python-Tetris
A simple Tetris-like game written in Python3 using Pygame.

## Table of contents
* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
	
## Description
The aim of a player is to place all the falling blocks so that they do not exit the board. When a row is full, then it disappears.
Player can control movement of falling blocks by pressing left/right arrows and rotate a block by pressing 'Shift'.
Program also saves high scores.

## Dependencies
Program requires following libraries to be installed:

* pygame
	
## Setup
Download Pygame:
```
$ pip3 install pygame
```

Run the program:
```
$ python3 Tetris.py
```

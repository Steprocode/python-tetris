"""Game based on classic Tetris.
You have to fit the blocks in such
a way to fill rows. Blocks accelerate
after each fit.

Created by Maciej Stępień"""

import pygame
import math
import sys
import random


def drawScanlines():
    """Draws partialy transparent patterns
    on the screen in old CRT monitor style."""
    global background, scr
    iter = 0
    thickness = 4
    height = scr.get_rect().height
    width = scr.get_rect().width
    surf = pygame.Surface((width, thickness))
    surf.set_alpha(64)

    while 2 * iter * thickness <= height:
        surf.fill(background)
        scr.blit(surf, (0, 2 * iter * thickness))
        iter += 1


def loadHighscore():
    """Loads highscore from a file if file
    exists, otherwise sets highcore to 0."""
    global highscore
    try:
        file = open("highscore.txt")
        words = file.readline().strip().split()
        highscore = int(words[0])
        file.close()
    except IOError:
        highscore = 0


def saveHighscore():
    """Saves highscore to file."""
    global highscore
    file = open('highscore.txt', 'w')
    file.write(str(highscore))
    file.close()


def drawBorderedBox(box):
    pygame.draw.rect(scr,
                     foreground,
                     box.get("outer_rect"))
    pygame.draw.rect(scr,
                     background,
                     getInnerRectOfBorderedBox(box))


def createBorderedBox(outerRect, border, borderColor, backgroundColor):
    borderedBox = {
        "outer_rect": outerRect,
        "border": border,
        "border_color": borderColor,
        "background_color": backgroundColor
    }
    return borderedBox


def getInnerRectOfBorderedBox(borderedBox):
    """Returns new calculated inner box
    of bordered box."""
    outerRect = borderedBox.get("outer_rect")
    border = borderedBox.get("border")
    return pygame.Rect(outerRect.x + border,
                       outerRect.y + border,
                       outerRect.width - 2 * border,
                       outerRect.height - 2 * border)


def createButton(text, font):
    """Return created Button - dictionary containing
    white and black text and also bounds."""
    blackText = font.render(str(text), True, background)
    whiteText = font.render(str(text), True, foreground)
    textBox = whiteText.get_rect()
    button = {
        "rect": textBox,
        "whiteText": whiteText,
        "blackText": blackText,
        "font": font,
        "text": text
    }
    return button


def rerenderButtonText(button):
    """Renders new fonts of button."""
    text = button.get("text")
    font = button.get("font")
    button["whiteText"] = font.render(str(text), True, foreground)
    button["blackText"] = font.render(str(text), True, background)


def drawButton(button):
    """Draws Button, and checks if mouse
    is inside bounds of Button."""
    global white, black
    border = 2
    rect = button.get("rect")
    if button.get("rect").collidepoint(pygame.mouse.get_pos()):
        pygame.draw.rect(
            scr,
            foreground,
            (rect.left - border,
             rect.top - border,
             rect.width + border * 2,
             rect.height + border * 2))
        scr.blit(button.get("blackText"), rect)
    else:
        pygame.draw.rect(
            scr,
            foreground,
            (rect.left - border * 2,
             rect.top - border * 2,
             rect.width + border * 4,
             rect.height + border * 4))
        pygame.draw.rect(
            scr,
            background,
            (rect.left - border,
             rect.top - border,
             rect.width + border * 2,
             rect.height + border * 2))
        scr.blit(button.get("whiteText"), rect)


def isButtonPressed(button):
    """Checks if Button is pressed and
    returns True or False."""
    global events
    if pygame.MOUSEBUTTONUP in events\
       and button.get("rect").collidepoint(pygame.mouse.get_pos()):
            return True
    return False


def handleInput():
    """Adds detected events to the global
    list of events. Method should be called
    in every tick."""
    global events
    events = []
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            events.append(pygame.MOUSEBUTTONUP)
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                events.append(pygame.K_LEFT)
            if event.key == pygame.K_RIGHT:
                events.append(pygame.K_RIGHT)
            if event.key == pygame.K_LSHIFT:
                events.append(pygame.K_LSHIFT)
            if event.key == pygame.K_ESCAPE:
                events.append(pygame.K_ESCAPE)

    keys = pygame.key.get_pressed()

    if keys[pygame.K_s]:
        events.append(pygame.K_s)
    if keys[pygame.K_w]:
        events.append(pygame.K_w)
    if keys[pygame.K_UP]:
        events.append(pygame.K_UP)
    if keys[pygame.K_DOWN]:
        events.append(pygame.K_DOWN)


def showGameOverScreen(score):
    global scr, clock, highscore

    if highscore < score:
        highscore = score
        saveHighscore()

    spacing = round(48 * scale)
    overText = bigFont.render("Game over", True, foreground)
    overBox = overText.get_rect()
    overBox.center = fitScr.center
    overBox.top = fitScr.top + spacing

    scoreText = font.render("Score: ", True, foreground)
    scoreBox = scoreText.get_rect()
    scoreBox.top = overBox.bottom + spacing

    scoreboard = font.render(str(score), True, foreground)
    scoreboardBox = scoreText.get_rect()
    scoreboardBox.top = scoreBox.top

    deviation = (scoreBox.width + scoreboardBox.width) / 2
    scoreBox.left = round(scr.get_rect().width / 2 - deviation)
    scoreboardBox.left = scoreBox.right

    retryButton = createButton("Retry", bigFont)
    continueButton = createButton("Menu", bigFont)
    exitButton = createButton("Exit", bigFont)

    retryButton.get("rect").center = scr.get_rect().center
    continueButton.get("rect").center = scr.get_rect().center
    exitButton.get("rect").center = scr.get_rect().center

    retryButton.get("rect").top = scoreBox.bottom + spacing
    continueButton.get("rect").top =\
        retryButton.get("rect").bottom + spacing
    exitButton.get("rect").top =\
        continueButton.get("rect").bottom + spacing
    while True:
        handleInput()

        if isButtonPressed(retryButton):
            showPlayScreen()
            break
        elif isButtonPressed(continueButton):
            showMenu()
            break
        if isButtonPressed(exitButton):
            sys.exit()

        scr.fill(background)
        drawButton(retryButton)
        drawButton(continueButton)
        drawButton(exitButton)
        scr.blit(overText, overBox)
        scr.blit(scoreText, scoreBox)
        scr.blit(scoreboard, scoreboardBox)
        drawScanlines()
        pygame.display.flip()
        clock.tick(60)


def showPauseScreen(score):
    global scr, clock, highscore

    if highscore < score:
        highscore = score
        saveHighscore()

    spacing = round(48 * scale)
    overText = bigFont.render("Pause", True, foreground)
    overBox = overText.get_rect()
    overBox.center = fitScr.center
    overBox.top = fitScr.top + spacing

    scoreText = font.render("Score: ", True, foreground)
    scoreBox = scoreText.get_rect()
    scoreBox.top = overBox.bottom + spacing

    scoreboard = font.render(str(score), True, foreground)
    scoreboardBox = scoreText.get_rect()
    scoreboardBox.top = scoreBox.top

    deviation = (scoreBox.width + scoreboardBox.width) / 2
    scoreBox.left = round(scr.get_rect().width / 2 - deviation)
    scoreboardBox.left = scoreBox.right

    continueButton = createButton("Continue", bigFont)
    menuButton = createButton("Menu", bigFont)
    exitButton = createButton("Exit", bigFont)

    continueButton.get("rect").center = scr.get_rect().center
    menuButton.get("rect").center = scr.get_rect().center
    exitButton.get("rect").center = scr.get_rect().center

    continueButton.get("rect").top =\
        scoreBox.bottom + spacing
    menuButton.get("rect").top =\
        continueButton.get("rect").bottom + spacing
    exitButton.get("rect").top =\
        menuButton.get("rect").bottom + spacing
    while True:
        handleInput()

        if isButtonPressed(continueButton)\
                or pygame.K_ESCAPE in events:
            break
        elif isButtonPressed(menuButton):
            showMenu()
            break
        if isButtonPressed(exitButton):
            sys.exit()

        scr.fill(background)
        drawButton(continueButton)
        drawButton(menuButton)
        drawButton(exitButton)
        scr.blit(overText, overBox)
        scr.blit(scoreText, scoreBox)
        scr.blit(scoreboard, scoreboardBox)
        drawScanlines()
        pygame.display.flip()
        clock.tick(60)


def showMenu():
    global scr, clock, foreground, background

    def onForegroundChange():
        nonlocal tetrisText, scoreText, changeText
        tetrisText = bigFont.render("TETRIS", True, foreground)
        scoreText = font.render("highscore: ", True, foreground)
        changeText = font.render("change:", True, foreground)
        rerenderButtonText(playButton)
        rerenderButtonText(exitButton)
        rerenderButtonText(foregroundButton)
        rerenderButtonText(backgroundButton)

    spacing = round(32 * scale)

    tetrisText = bigFont.render("TETRIS", True, foreground)
    tetrisBox = tetrisText.get_rect()
    tetrisBox.center = fitScr.center
    tetrisBox.top = fitScr.top + spacing

    playButton = createButton("Start game", bigFont)
    exitButton = createButton("Exit", bigFont)

    playButton.get("rect").center = scr.get_rect().center
    exitButton.get("rect").center = scr.get_rect().center

    playButton.get("rect").top = tetrisBox.bottom + spacing
    exitButton.get("rect").top = playButton.get(
        "rect").bottom + spacing

    scoreText = font.render("highscore: ", True, foreground)
    scoreBox = scoreText.get_rect()
    scoreBox.top = exitButton.get(
        "rect").bottom + spacing

    changeText = font.render("change:", True, foreground)
    changeBox = changeText.get_rect()
    changeBox.left = scoreBox.left
    changeBox.top = scoreBox.bottom + 6

    foregroundButton = createButton("foreground", font)
    backgroundButton = createButton("background", font)

    foregroundButton.get("rect").left = changeBox.right
    foregroundButton.get("rect").top = changeBox.bottom
    backgroundButton.get("rect").left = changeBox.right
    backgroundButton.get("rect").top =\
        foregroundButton.get("rect").bottom + spacing

    while True:
        handleInput()

        if isButtonPressed(playButton):
            showPlayScreen()
        if isButtonPressed(exitButton):
            sys.exit()
        if isButtonPressed(foregroundButton):
            idForeground = colors.index(foreground)
            idBackground = colors.index(background)
            idForeground += 1
            idForeground %= len(colors)
            if idBackground == idForeground:
                idForeground += 1
                idForeground %= len(colors)
            foreground = colors[idForeground]
            onForegroundChange()
        if isButtonPressed(backgroundButton):
            idForeground = colors.index(foreground)
            idBackground = colors.index(background)
            idBackground += 1
            idBackground %= len(colors)
            if idBackground == idForeground:
                idBackground += 1
                idBackground %= len(colors)
            background = colors[idBackground]
            onForegroundChange()

        scr.fill(background)
        drawButton(playButton)
        drawButton(exitButton)
        drawButton(foregroundButton)
        drawButton(backgroundButton)

        scoreboard = font.render(str(highscore),
                                 True,
                                 foreground)
        scoreboardBox = scoreText.get_rect()
        scoreboardBox.top = scoreBox.top

        deviation = (scoreBox.width + scoreboardBox.width) / 2
        scoreBox.left = round(scr.get_rect().width / 2 - deviation)
        scoreboardBox.left = scoreBox.right

        scr.blit(tetrisText, tetrisBox)
        scr.blit(scoreText, scoreBox)
        scr.blit(scoreboard, scoreboardBox)
        scr.blit(changeText, changeBox)
        drawScanlines()
        pygame.display.flip()
        clock.tick(60)


def showPlayScreen():
    global scr, clock, foreground, background, scale, fitScr

    def createGameArea(x, y, blockSize):
        """Returns new dictionary containing information
        with properties necessary to handle game status."""
        elements = []

        playRect = pygame.Rect(x, y, blockSize * 10 + border * 13,
                               blockSize * 20 + border * 23)
        playBox = createBorderedBox(playRect, border, foreground,
                                    background)

        for n in range(0, 10):
            elements.append([0, 0, 0, 0, 0,
                             0, 0, 0, 0, 0,
                             0, 0, 0, 0, 0,
                             0, 0, 0, 0, 0])
        gameArea = {
            "bordered_box": playBox,
            "elements": elements,
            "block_size": blockSize
        }
        return gameArea

    def createQueueArea(x, y, size, border):
        """Returns new dictionary containing information
        with properties necessary to handle queue view."""
        structs = []
        queueRect = pygame.Rect(x, y, size * 4 + border * 7,
                                size * 12 + border * 19)
        queueBox = createBorderedBox(queueRect, border,
                                     foreground, background)

        queueArea = {
            "bordered_box": queueBox,
            "structs": structs,
            "block_size": size,
            "border": border
        }
        return queueArea

    def drawQueueArea(queueArea):
        """Draws queue area."""
        drawBorderedBox(queueArea.get("bordered_box"))
        border = queueArea.get("border")
        size = queueArea.get("block_size")
        x = queueArea.get("bordered_box").get("outer_rect").x
        y = queueArea.get("bordered_box").get("outer_rect").y

        for i in range(0, 3):
            pygame.draw.rect(scr,
                             foreground,
                             (x,
                              y + border * (border * i) +
                              i * (size + border) * 4,
                              queueArea.get("bordered_box").get(
                                  "outer_rect").width,
                              border))
            drawStruct(x + border,
                       y + border * (1 + border * i) +
                       i * (size + border) * 4,
                       border,
                       size,
                       queueArea.get("structs")[i])

    def drawGameArea(gameArena):
        """Draws game area."""
        drawBorderedBox(gameArena.get("bordered_box"))
        area = getInnerRectOfBorderedBox(gameArea.get("bordered_box"))
        border = gameArena.get("bordered_box").get("border")
        blockSize = gameArena.get("block_size")
        for x in range(0, 10):
            for y in range(0, 20):
                if gameArena.get("elements")[x][y] == 1:
                    pygame.draw.rect(scr,
                                     foreground,
                                     [area.x + border + (border +
                                      blockSize) * x,
                                      area.y + border +
                                      (border + blockSize) * y,
                                      blockSize,
                                      blockSize])

    def drawStruct(screenX, screenY, border, size, struct):
        """Draws single structure."""
        for x in range(0, len(struct)):
            for y in range(0, len(struct[0])):
                if struct[x][(len(struct[0]) - y - 1)] == 1:
                    pygame.draw.rect(scr,
                                     foreground,
                                     [screenX + border +
                                      (border + blockSize) * x,
                                      screenY + border +
                                      (border + blockSize) * y,
                                      size,
                                      size])

    def rotateStruct(rotates, struct):
        """Rotates structure by 90 degrees multiplied
        by rotates' number."""
        while rotates < 0:
            rotates += 4
        rotates = rotates % 4
        if rotates == 0:
            return struct

        width = len(struct)
        height = len(struct[0])

        newStruct = []
        for y in range(0, height):
            newStruct.append([])
            for x in range(0, width):
                newStruct[y].append(struct[width - x - 1][y])

        rotates -= 1
        struct = newStruct

        if rotates > 0:
            struct = rotateStruct(rotates, struct)
        return struct

    def createBlock(gameArea):
        """Creates new dictionary containing information
        necessary to describe block status."""
        struct = queue[0]
        queue.append(randomStruct())
        del queue[0]
        width = len(struct)
        randXPos = random.randint(0, len(gameArea.get("elements")) - width)

        block = {
            "struct": struct,
            "position": [randXPos, 0]
        }
        return block

    def randomStruct():
        """Creates random duplicated structure
        with random rotation."""
        randBlock = random.randint(0, 6)
        struct = copyBlock(blocks[randBlock])
        rotation = random.randint(0, 4)
        struct = rotateStruct(rotation, struct)
        return struct

    def rotateBlock(rotates, block, gameArea):
        """Rotates block if it is possible."""
        while rotates < 0:
            rotates += 4
        rotates = rotates % 4
        if rotates == 0:
            return

        struct = block.get("struct")
        width = len(struct)
        height = len(struct[0])

        newStruct = []
        for y in range(0, height):
            newStruct.append([])
            for x in range(0, width):
                newStruct[y].append(struct[width - x - 1][y])

        rotates -= 1
        block["struct"] = newStruct

        if rotates > 0:
            rotateBlock(rotates, block, gameArea)
        if rotates == 0 and checkCollision(False, block, gameArea):
            block["struct"] = struct
            return

    def copyBlock(struct):
        """Returns dupicated block"""
        width = len(struct)
        height = len(struct[0])
        newStruct = []
        for x in range(0, width):
            newStruct.append([])
            for y in range(0, height):
                newStruct[x].append(struct[x][y])
        return newStruct

    def drawBlock(block, gameArea):
        """Draws block."""
        area = gameArea.get("bordered_box").get("outer_rect")
        border = gameArea.get("bordered_box").get("border")
        blockSize = gameArea.get("block_size")
        for x in range(0, len(block.get("struct"))):
            for y in range(0, len(block.get("struct")[0])):
                areaX = x + block.get("position")[0]
                areaY = block.get("position")[1] - y
                if y >= 0 and block.get("struct")[x][y] == 1\
                        and areaY >= 0:
                    pygame.draw.rect(scr,
                                     foreground,
                                     [area.x + border +
                                      (border + blockSize) * areaX,
                                      area.y + border +
                                      (border + blockSize) * areaY,
                                      blockSize,
                                      blockSize])

    def checkCollision(isTopColliding, block, gameArea):
        """Checks if block collides with elements
        in game area."""
        elements = gameArea.get("elements")
        for x in range(0, len(block.get("struct"))):
            for y in range(0, len(block.get("struct")[0])):
                areaX = x + block.get("position")[0]
                areaY = block.get("position")[1] - y
                # check if element of block is inside bounds
                if areaX >= len(elements)\
                        or areaX < 0\
                        or areaY >= len(elements[0])\
                        or (isTopColliding and areaY < 0):
                    return True
                # check if element of block doesn't collide with the others
                if y >= 0 and block.get("struct")[x][y] == 1\
                        and areaY >= 0:
                    if elements[areaX][areaY] == 1\
                            and block.get("struct")[x][y] == 1:
                        return True
        return False

    def eliminateRow(row, gameArea):
        """Removes row of elements in game area."""
        elements = gameArea.get("elements")
        width = len(elements)
        for y in range(row - 1, -1, -1):
            for x in range(0, width):
                elements[x][y + 1] = elements[x][y]

        for x in range(0, width):
            elements[x][0] = 0

    def moveBlock(direction, block, gameArea):
        """Moves block by one unit in left, right
        or down direction."""
        if direction == "left":
            block.get("position")[0] -= 1
            collides = checkCollision(False, block, gameArea)
            if collides:
                block.get("position")[0] += 1
                return False

        if direction == "right":
            block.get("position")[0] += 1
            collides = checkCollision(False, block, gameArea)
            if collides:
                block.get("position")[0] -= 1
                return False

        if direction == "down":
            block.get("position")[1] += 1
            collides = checkCollision(False, block, gameArea)
            if collides:
                block.get("position")[1] -= 1
                return False

        return True

    def mergeBlock(block, gameArea):
        """Anchors block in game area."""
        elements = gameArea.get("elements")
        for x in range(0, len(block.get("struct"))):
            for y in range(0, len(block.get("struct")[0])):
                areaX = x + block.get("position")[0]
                areaY = block.get("position")[1] - y
                if y >= 0 and block.get("struct")[x][y] == 1 and areaY >= 0:
                    elements[areaX][areaY] = block.get("struct")[x][y]

    def onDownCollision():
        """Takes proper action if downside collision
        is detected."""
        nonlocal block, gameArea, running, downTime, score
        if checkCollision(True, block, gameArea):
            running = False
        mergeBlock(block, gameArea)
        elements = gameArea.get("elements")
        width = len(elements)
        height = len(elements[0])
        for y in range(0, height):
            fullRow = True
            for x in range(0, width):
                if elements[x][y] == 0:
                    fullRow = False
                    break
            if fullRow:
                eliminateRow(y, gameArea)
                score += 10

        if running:
            block = createBlock(gameArea)
            score += 4
            if downTime > 200:
                downTime -= 2

    blocks = [
        [[0, 0, 1],
         [1, 1, 1]],

        [[1, 1, 1],
         [0, 0, 1]],

        [[0, 1, 0],
         [1, 1, 1]],

        [[1, 1, 1, 1]],

        [[1, 1, 0],
         [0, 1, 1]],

        [[0, 1, 1],
         [1, 1, 0]],

        [[1, 1],
         [1, 1]]
    ]

    # entry settings of the game
    border = round(scale * 2)
    blockSize = round(30 * scale)

    gameArea = createGameArea(0, 0, blockSize)
    gameArea.get("bordered_box").get("outer_rect").left =\
        fitScr.left + border
    gameArea.get("bordered_box").get("outer_rect").bottom =\
        fitScr.bottom - border

    queueArea = createQueueArea(0, 0, blockSize, border)
    queueArea.get("bordered_box").get("outer_rect").left =\
        gameArea.get("bordered_box").get("outer_rect").right + border
    queueArea.get("bordered_box").get("outer_rect").top =\
        gameArea.get("bordered_box").get("outer_rect").top
    queue = []
    for i in range(0, 3):
        queue.append(randomStruct())
    queueArea["structs"] = queue

    block = createBlock(gameArea)
    running = True

    scoreText = font.render("Score:", True, foreground)
    scoreBox = scoreText.get_rect()
    scoreBox.left =\
        gameArea.get("bordered_box").get("outer_rect").right + border
    scoreBox.top =\
        queueArea.get("bordered_box").get("outer_rect").bottom + border
    score = 0

    pauseButton = createButton("||", bigFont)
    pauseButton.get("rect").center = scoreBox.center

    downTime = 500
    downTimeCounter = 0
    
    eventTime = 0
    eventWaitTime = 2
    while True:
        if downTimeCounter >= downTime:
            downTimeCounter = 0
            if not moveBlock("down", block, gameArea):
                onDownCollision()

        if running:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        showPauseScreen(score)
                    if event.key == pygame.K_LSHIFT:
                        rotateBlock(1, block, gameArea)
                    if event.key == pygame.K_DOWN:
                        if not moveBlock("down", block, gameArea):
                            onDownCollision()
                    if eventTime == 0:
                        eventTime = eventWaitTime
                        if event.key == pygame.K_LEFT:
                            moveBlock("left", block, gameArea)
                        if event.key == pygame.K_RIGHT:
                            moveBlock("right", block, gameArea)
                    
            if isButtonPressed(pauseButton):
                showPauseScreen(score)
        else:
            showGameOverScreen(score)
            break
        
        # draw game elements and gui
        scr.fill(background)
        drawGameArea(gameArea)
        drawQueueArea(queueArea)
        scr.blit(scoreText, scoreBox)

        scoreboard = font.render(str(score), True, foreground)
        scoreboardBox = scoreText.get_rect()
        scoreboardBox.left =\
            gameArea.get("bordered_box").get("outer_rect").right + border
        scoreboardBox.top = scoreBox.bottom + border
        pauseButton.get("rect").top = scoreboardBox.bottom + border
        scr.blit(scoreboard, scoreboardBox)
        drawButton(pauseButton)

        if running:
            drawBlock(block, gameArea)
        drawScanlines()
        pygame.display.flip()
        downTimeCounter += clock.tick(60)
        eventTime -= 1
        eventTime = max(eventTime, 0)
        


if __name__ == "__main__":
    global foreground, background, scr, clock, modeType, events,\
        scale, blank, font, bigFont, fitScr, colors
    colors = ((0, 0, 0),
              (255, 255, 255),
              (255, 0, 0),
              (0, 255, 0),
              (0, 0, 255),
              (255, 255, 0),
              (255, 0, 255),
              (0, 255, 255))

    defaultScrDim = (470, 650)
    events = []
    loadHighscore()
    pygame.init()
    background = colors[0]
    foreground = colors[1]

    font = pygame.font.Font('Jamma.ttf', 40)
    bigFont = pygame.font.Font('Jamma.ttf', 70)
    scr = pygame.display.set_mode(defaultScrDim)

    scales = (scr.get_rect().width / defaultScrDim[0],
              scr.get_rect().height / defaultScrDim[1])
    if scales[0] > scales[1]:
        scale = scales[1]
        blank = (round(math.fabs(
            scr.get_rect().width - defaultScrDim[0]) / 2), 0)
    elif scales[1] > scales[0]:
        scale = scales[0]
        blank = (0, round(
            math.fabs(scr.get_rect().height - defaultScrDim[1]) / 2))
    else:
        scale = 1
        blank = (0, 0)

    fitScr = pygame.Rect(0,
                         0,
                         defaultScrDim[0] * scale,
                         defaultScrDim[1] * scale)
    fitScr.center = scr.get_rect().center

    pygame.key.set_repeat(200, 50)
    clock = pygame.time.Clock()
    showMenu()
